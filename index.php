<?php
require "vendor/autoload.php";
$app = new \Slim\App();
$app->get('/customer/{number}', function($request, $response,$args){
    $json = '{"1":"Prili", "2":"Dor"}';
    $array = (json_decode($json, true));
    if(array_key_exists($args['number'], $array)){
        echo $array[$args['number']];
    }
    else{
        echo "This user not exist";
    }
});
$app->run();